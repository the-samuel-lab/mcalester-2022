//Cell processing macro for ImageJ v.2.3.0/1.53f

//Select folder to use and create output folder
run("Clear Results");
if (roiManager("Count") > 0){
	roiManager("Select All");
	roiManager("Delete");
}
waitForUser("Please select the folder that contains your raw microscopy pictures");
RawInput = getDirectory("Please select the folder that contains your raw microscopy pictures");
list = getFileList(RawInput);
Soutput=RawInput+"Processor_Output/";
File.makeDirectory(Soutput)
File.makeDirectory(Soutput+"Export/")
File.makeDirectory(Soutput+"CellMontage/")
File.makeDirectory(Soutput+"csv/")

//First set thresholds
run("Bio-Formats Importer", "open=" + RawInput + list[0] + " autoscale color_mode=Default view=Hyperstack stack_order=XYCZT");

getDimensions(width, height, channels, slices, frames);
print(channels);
name=getTitle();
Table.create("Tsetting");

n=getNumber("Enter minimum cell area in micron^2", 30);
rn=getNumber("Enter minimum signal area in micron^2", 5);
//gn=getNumber("Enter minimum signal area in micron^2", 5);
//rmax=getNumber("Enter maximum red signal area in micron^2", 30);
npic=getNumber("Enter number of image stored in nd2 file", 1);

for(j=1;j<channels+1;j++){
	setSlice(j);
	run("Subtract Background...", "rolling=100");
	run("Threshold...");
	resetThreshold();
	setAutoThreshold("Otsu dark");
	waitForUser("Adjust B&C for channel "+j+" THEN press ok");
	getThreshold(lower,upper);
	Table.set("Min", j-1 , lower);
	Table.set("Max", j-1 , upper);
	Table.set("Channel", j-1, j);
	Table.update;
}

run("Close All");

//Process files
for (i=0; i<list.length; i++){
	if(endsWith(list[i], ".nd2")){
	run("Bio-Formats Importer", "open="+RawInput+list[i] +  " autoscale color_mode=Default view=Hyperstack stack_order=XYCZT");
	for(v=0;v<npic; v++){ 
		name=getTitle();
		name=replace(name, ".nd2", "");
		File.makeDirectory(Soutput+"Export/"+name)
		//Looking at signal overlay
		run("Duplicate...", "duplicate channels=4");
		run("Subtract Background...", "rolling=100");
		setAutoThreshold("Otsu dark");
		//run("Threshold...");
		setThreshold(Table.get("Min", 3), Table.get("Max", 3));
		run("Convert to Mask");
		run("Despeckle");
		run("Smooth");
		run("Convert to Mask");
		run("Watershed");
		run("Analyze Particles...", "size="+n+"-Infinity display exclude include add in_situ");
		run("Clear Results");
		close();
		if (roiManager("Count") > 0){
			//extract single cells
			for (c=1; c<channels+1;c++){
				Stack.setChannel(c);	
				run("Subtract Background...", "rolling=100");
			}
			for (j=0; j<roiManager("count"); ++j) {
	    		roiManager("Select", j);
	    		run("Duplicate...", "duplicate");
	    		saveAs("Tiff", Soutput+"Export/"+name+File.separator+name+"-cell-"+j+".tif");
	        	close();
			}
			//Process cells
			cellList = getFileList(Soutput+"Export/"+name+File.separator);
			roiManager("Select All");
			roiManager("Delete");
			for (j=0; j<cellList.length; j++){
				open(Soutput+"Export/"+name+File.separator+cellList[j]);
				cellName=getTitle();
				cellName=replace(cellName, ".tif", "");
				//Remove unspecific signal
				setBackgroundColor(0, 0, 0);
				run("Clear Outside", "stack");
				//get specitif signal
				for (c=1; c<channels+1;c++){
					Stack.setChannel(c);
					run("Set Measurements...", "area mean min display redirect=None decimal=0");
					run("Measure");
					setResult("C7", c-1, "General");
				}
				x=nResults;
				for (c=1; c<channels+1;c++){
					Stack.setChannel(c);
					//run("Threshold...");
					setThreshold(Table.get("Min", c-1), Table.get("Max", c-1));
					run("Analyze Particles...", "size="+rn+"-Infinity pixel display include add in_situ");
					if (roiManager("Count") > 0){
						roiManager("Select All");
						roiManager("Delete");
					}
				if (nResults>x){
					for (z=x; z<nResults;z++){
						setResult("C7", z, "Special");
						}
					}
				}
				saveAs("Results", Soutput+"csv/"+cellName+".csv");
				run("Clear Results");
				Stack.setDisplayMode("composite");
				saveAs("Jpeg", Soutput+"CellMontage/"+cellName+"Jpeg");
				close();
				}	
			}
		run("Collect Garbage");	
		close();
		}	
	}	
}
